### Coding Style
	
	

### Files Responsibility
- .htaccess file in root is responsible for redirecting all request to public folder
- .htaccess file in public folder is responsible for restrict direct access and redirect request to index.php, which is the **single entry point** of our Application

### Application work flow
	All the GET Request will reach the application from a single point, the *index.php* file in the public folder. 
	*index.php* will extract the url and then load the *bootstrap.php*.
	*bootstrap.php* file will load all the basic class and config data. And then pass the **url** to the Router.
	Every Single GET request will be in the form of **"http://siteurl/controllerName/methodName/queryParameters"**
	Router will extract this informations from url and then call the related controller to take further steps.
	From this point the real MVC flow will start taking place.