<?php

class homecontroller extends Controller{



	public function __construct()
	{
		//$this->$_action = $action;

		$this->model = new homemodel();
	}

	public function index()
	{
		//echo 'home sweet home';

		$homepage = array();
		// array_push($homepage, 'header');
		// array_push($homepage, 'home');
		// array_push($homepage, 'footer');

		$categories = $this->model->getCategories();

		array_push($homepage, 'home-metahead');
		array_push($homepage, 'home-header');
		array_push($homepage,'homepage');
		array_push($homepage,'home-footer');

		View::makeView($homepage,null,$categories);
	}


	public function changelang($lang)
	{
		Trans::changeLang($lang);

		Redirect::to('');
	}

}