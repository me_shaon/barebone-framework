<?php

class logincontroller extends Controller{

	public function __construct()
	{
		//$this->$_action = $action;

		$this->model = new usermodel();
	}

	public function index()
	{
		logincontroller::login();
	}

	private function create_login()
	{
		$loginpage = array();
		array_push($loginpage, 'login-header');
		array_push($loginpage, 'login');

		return $loginpage;
	}

	private function create_register()
	{
		$registerpage = array();
		array_push($registerpage, 'login-header');
		array_push($registerpage, 'register');

		return $registerpage;
	}


	public function login($attempt=null)
	{
		$loginpage = self::create_login();

		if(!strcasecmp($attempt, 'success'))
			View::makeView($loginpage,['success'=>'Registration successful, Please Log in']);
		else
			View::makeView($loginpage);
	}

	public function register()
	{
		$registerpage = self::create_register();

		View::makeView($registerpage);
	}


	public function postlogin()
	{
		if($this->model->loginSuccess() == true)
		{
			Redirect::to('');
		}
		else
		{
			$loginpage = self::create_login();

			View::makeView($loginpage,['error'=>'Username or Password not matched']);
		}
	}

	public function postregister()
	{
		if($this->model->registerSuccess() == true)
		{
			Redirect::to('logincontroller/login/success');
		}
		else
		{
			$registerpage = self::create_register();

			View::makeView($registerpage,['error'=>'User already Exists']);
		}
	}


	public function logout()
	{
		if(Auth::userLoggedIn())
		{
			Auth::logOut();
		}

		Redirect::to('');
	}


}