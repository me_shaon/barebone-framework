<?php

// Load configuration and helper functions
require_once(ROOT . DS . 'arch' . DS . 'config' . DS . 'config.php');
require_once(ROOT . DS . 'arch' . DS . 'config' . DS . 'app.php');
require_once(ROOT . DS . 'lib' . DS . 'helper' . DS . 'functions.php');


// Autoload classes
function __autoload($className)
{
	if(file_exists(ROOT . DS. 'lib'. DS . 'core' . DS . strtolower($className) . '.php'))
	{
		require_once(ROOT . DS. 'lib'. DS . 'core' . DS . strtolower($className) . '.php');
	}
	else if(file_exists(ROOT . DS. 'application'. DS . 'controllers' . DS . strtolower($className) . '.php'))
	{
		require_once(ROOT . DS. 'application'. DS . 'controllers' . DS . strtolower($className) . '.php');
	}
	else if(file_exists(ROOT . DS. 'application'. DS . 'models' . DS . strtolower($className) . '.php'))
	{
		require_once(ROOT . DS. 'application'. DS . 'models' . DS . strtolower($className) . '.php');
	}
	else if(file_exists(ROOT . DS. 'lib'. DS . 'utility' . DS . strtolower($className) . '.php'))
	{
		require_once(ROOT . DS. 'lib'. DS . 'utility' . DS . strtolower($className) . '.php');
	}
}


// Route the request
Router::route($url);