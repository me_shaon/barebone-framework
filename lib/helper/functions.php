<?php

session_start();

/** Checking Development environment and displaying error settings **/
function setReporting()
{
	if (APP_DEBUG == true)
	{
		error_reporting(E_ALL);
		ini_set('display_errors','On');
	}
	else
	{
		error_reporting(E_ALL);
		ini_set('display_errors','Off');
		ini_set('log_errors', 'On');
		ini_set('error_log', ROOT.DS.'tmp'.DS.'logs'.DS.'error.log');
	}
}

setReporting();
