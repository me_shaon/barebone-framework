<?php


class Auth
{
	public static function login($user)
	{

		foreach ($user as $key => $value)
		{
			$user[$key] = $value;
		}

		$_SESSION['user'] = $user;
	}


	public static function userLoggedIn()
	{
		if(isset($_SESSION['user']['name']))
			return true;
		return false;
	}

	public static function get($key)
	{
		return $_SESSION['user'][$key];
	}


	public static function logOut()
	{
		session_unset();

		session_destroy();
	}
}