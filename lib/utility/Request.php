<?php

class Request{

	private $data;

	public static function all()
	{
		$data = array();

		if($_SERVER['REQUEST_METHOD']=='POST')
	  	{
	  		//validating input information
	  		foreach ($_POST as $key => $value)
	  		{
	  			if(isset($_POST[$key]))
	  				$data[$key] = self::input_validate($value);
	  		}
	  	}
	  	else if($_SERVER['REQUEST_METHOD']=='GET')
	  	{
	  		//validating input information
	  		foreach ($_GET as $key => $value)
	  		{
	  			if(isset($_GET[$key]))
	  				$data[$key] = self::input_validate($value);
	  		}
	  	}

	  	return $data;
	}


	public static function has($key)
	{
		if(isset($_POST[$key]) || isset($_GET[$key]))
			return true;

		return false;
	}


	public static function get($key)
	{
		$data = null;

		if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST[$key]))
	  	{
	  		$data = self::input_validate($_POST[$key]);
	  	}
	  	else if($_SERVER['REQUEST_METHOD']=='GET' && isset($_GET[$key]))
	  	{
	  		$data = self::input_validate($_GET[$key]);
	  	}

	  	return $data;
	}

	private static function input_validate($value)
  	{
  		  $data = trim($value);
  		  $data = stripslashes($value);
  		  $data = htmlspecialchars($value);
  		  return $value;
  	}

}