<?php

class Trans
{
	public static function text($key)
	{
		if(!isset($_SESSION['lang']))
			$_SESSION['lang'] = DEFAULT_LANG;

		$lang = $_SESSION['lang'];

		$langpath = ROOT.DS.'arch'.DS.'lang'.DS.$lang.DS.'page.php';

		$langdata = include($langpath);

		if(!array_key_exists($key, $langdata))
			echo "";
		else
			echo $langdata[$key];
	}


	public static function changeLang($locale)
	{
		$_SESSION['lang'] = $locale;
	}

	public static function lang()
	{
		if(!isset($_SESSION['lang']))
			$_SESSION['lang'] = DEFAULT_LANG;

		return $_SESSION['lang'];
	}


}
