<?php
/*
	Helper Class for Query management
 */
class SQL
{
	private $connector;


	public static function raw($sql)
	{
		$connector = new DBConnector(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);

		$querytype = explode(' ', $sql);

		if(!strcasecmp($querytype[0],'insert')) //insert query
		{
			$result = $connector->insertData($sql);
		}
		else if(!strcasecmp($querytype[0],'delete')) //delete query
		{
			$result = $connector->deleteData($sql);
		}
		else if(!strcasecmp($querytype[0],'update')) //update query
		{
			$result = $connector->deleteData($sql);
		}
		else if(!strcasecmp($querytype[0],'select')) //select query
		{
			$result = $connector->selectData($sql);
		}

		return $result;
	}

}