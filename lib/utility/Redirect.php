<?php

class Redirect{

	public static function to($url, $permanent = false)
	{
		if($permanent)
		{
			header('HTTP/1.1 301 Moved Permanently');
		}

		$url = APP_URL.$url;

		header('Location: '.$url);
		exit();
	}

}