<?php


class HTML{

	public static function style($href,$attr=null)
	{
		echo "<link href='";
		echo APP_URL.$href;
		echo "'";

		if($attr)
		{
			foreach ($attr as $key => $value)
			{
				echo " ".$key;
				echo "='";
				echo $value;
				echo "' ";
			}
		}

		echo " rel='stylesheet' type='text/css' />";
		echo "\n";
	}

	public static function image($href,$attr=null)
	{
		echo "<img src='";
		echo APP_URL.$href;
		echo "'";

		if($attr)
		{
			foreach ($attr as $key => $value)
			{
				echo " ".$key;
				echo "='";
				echo $value;
				echo "' ";
			}
		}

		echo " />";
		echo "\n";
	}

	public static function script($href,$attr=null)
	{
		echo "<script src='";
		echo APP_URL.$href;
		echo "'";

		if($attr)
		{
			foreach ($attr as $key => $value)
			{
				echo " ".$key;
				echo "='";
				echo $value;
				echo "' ";
			}
		}

		echo " ></script>";
		echo "\n";
	}


	/**
	 * [This function will get an array consisting of the information of a  <menu> element of the XML and try to create formatted HTML output]
	 * This function is implemented in a recursive way, try to understand how it really works
	 * @param  [array] $menu [this array holds all info of an <menu> tag in the XML file]
	 */
	public static function print_menu($menu)
	{
		if(sizeof($menu)>0) //If the given element is not empty
		{
			echo "<ul"; //As we have an element to work with, start with printing the <ul> tag
			//echo "<br>";

			if(array_key_exists("class", $menu)) //if css class exists
			{
				echo " class='";
				echo $menu['class'];
				echo "' ";
			}

			if(array_key_exists("id", $menu)) //if css id exists
			{
				echo " id='";
				echo $menu['id'];
				echo "' ";
			}

			echo ">";

			for($i = 0;$i<sizeof($menu["elem"]);$i++) //If there is <elem> inside this <menu>
			{
				$elem = $menu["elem"][$i];

				if(array_key_exists("title", $elem)) //Checking the existence of <title> tag
				{
					echo "<li>"; //We have title to print, so first print <li>
					$link = false;

					if(array_key_exists("link", $elem)) //We also have link so print the link with <a> tag
					{
						$link = true;
						echo "<a href='";
						echo $elem["link"];
						echo "'>";
					}

					$langfile = Trans::lang();
					echo $elem["title"][$langfile]; //after printing the <a> opening tag, print the title

					if($link == true) //now print the closing </a> tag
						echo "</a>";

					//echo "<br>";
					if(array_key_exists("menu", $elem)) //If there is submenu to this menu, send it to the same function for recursively print its elements too
						self::print_menu($elem["menu"]);

					echo "</li>"; //close the </li> tag
					//echo "<br>";
				}

			}
			echo "</ul>"; //close the </ul> tag
			//echo "<br>";
		}
	}


	public static function create_menu($filename)
	{
		 $filename = APP_URL.$filename;

		 $xml = simplexml_load_file($filename); //Load the xml file

		$menus = $xml->xpath('/mainmenu'); //Read all data inside <mainmenu> tag using xpath()

		//return value of the xpath() function is simpleXMLObject, we need to convert it to array
		$menuarray = json_decode(json_encode((array)$menus),true); //converting simpleXMLObject to jsron Array


		//convert all the menu using print_menu function, implemented above
		for($m = 0; $m < sizeof($menuarray[0]); $m++)
		{
			self::print_menu($menuarray[0]["menu"]);
		}

	}
}