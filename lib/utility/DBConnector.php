<?php

class DBConnector
{
	private $servername;
	private $username;
	private $password;
	private $dbname;
	private $conn;


	public function __construct($server = NULL, $user = NULL, $pass = NULL, $db = NULL)
	{
		$this->servername 	= $server?$server:'localhost';
		$this->username 	= $user?$user:'root';
		$this->password 	= $pass?$pass:'';
		$this->dbname 		= $db?$db:'test';
	}

	private function executeQuery($sql)
	{
		$this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

		mysqli_query($this->conn,'SET CHARACTER SET utf8');
		mysqli_query($this->conn,"SET SESSION collation_connection ='utf8_general_ci'");

		$result = null;

		if ($this->conn->connect_error)
		{
		    die("Connection failed: " . $this->conn->connect_error);
		}

		$result = $this->conn->query($sql);

		$this->conn->close();

		return $result;
	}

	public function insertData($sql)
	{
		if($sql)
		{
			$result = $this->executeQuery($sql);
			if ($result)
			{
				//echo "Data inserted successfully";

				return true;
			}
			else
			{
			    echo "Error: " . $sql . "<br>" . $this->conn->error;

			    return false;
			}
		}
		else
		{
			die('Wrong Insert Query: $sql');
		}
	}

	public function deleteData($sql)
	{
		if($sql)
		{
			$result = $this->executeQuery($sql);
			if ($result)
			{
				//echo "Data deleted successfully";

				return true;
			}
			else
			{
			    echo "Error: " . $sql . "<br>" . $this->conn->error;

			    return false;
			}
		}
		else
		{
			die('Wrong Delete Query: $sql');
		}
	}

	public function selectData($sql)
	{
		if($sql)
		{
			$result = $this->executeQuery($sql);
			$rows = array();

			while($row = $result->fetch_assoc())
			{
				array_push($rows, $row);
			}

			return $rows;
		}
		else
		{
			die('Wrong Select Query: $sql');
		}
	}

	public function updateData($sql)
	{
		if($sql)
		{
			$result = $this->executeQuery($sql);
			if ($result)
			{
				//echo "Data updated successfully";

				return true;
			}
			else
			{
			    echo "Error: " . $sql . "<br>" . $this->conn->error;

			    return false;
			}
		}
		else
		{
			die('Wrong Update Query: $sql');
		}
	}

}


?>
