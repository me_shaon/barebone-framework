<?php

class View{

	protected static $viewparts;

	protected static $viewpath;

	public static function init()
	{
		self::$viewpath = ROOT . DS . 'arch' . DS . 'views' . DS;

		self::$viewparts = array();

		$GLOBALS['error'] = null;
		$GLOBALS['message'] = null;
	}

	public static function render()
	{
		foreach (self::$viewparts as $part)
		{
			include($part);
		}
	}

	public static function addParts($bodypart)
	{
		array_push(self::$viewparts, $bodypart);
	}

	public static function makeView($parts,$message = null,$viewdata=null)
	{
		self::init();

		foreach ($parts as $part)
		{
			if(file_exists(self::$viewpath.$part.'.php'))
				self::addParts(self::$viewpath.$part.'.php');
		}

		if($message)
		{
			if(array_key_exists('error', $message))
			{
				$GLOBALS['error'] = $message['error'];
			}

			if(array_key_exists('success', $message))
			{
				$GLOBALS['message'] = $message['success'];
			}
		}

		$_SESSION['data'] = $viewdata;

		self::render();
	}

}