<?php

class Router{

	public static function route($url)
	{
		//echo "I am routing to ".$url;

		$error = false;

		$url_parts = array();
		$url_parts = explode("/",$url);

		$controller_name = empty($url_parts[0])?DEFAULT_CONTROLLER:$url_parts[0];

		array_shift($url_parts);

		$method = isset($url_parts[0]) && !empty($url_parts[0])?$url_parts[0]:'index';

		array_shift($url_parts);

		$parameters = $url_parts;


		if(class_exists($controller_name))
		{
			$controller = new $controller_name();

			if(method_exists($controller, $method))
			{
				call_user_func_array(array($controller,$method), $parameters);
			}

		}
		else
		{
			echo "Page not found";
		}

	}
}