<?php


return [

	'welcome' 			=> 'Welcome to Book House',
	'welcomesubtext' 	=> 'The Only Book Store where you would find all of your favorite books online.',
	'login'				=> 'Login',
	'register'			=> 'Register',
	'logout'			=> 'LogOut',
	'hello'				=> 'Hello',
	'bestbooks'			=> 'Best Books',
	'bestbooksubtext'	=> 'Most of the best seller books are available here.',
	'bestseller'		=> 'Best Seller Books',
	'buynow'			=> 'Buy Now',
	'menu'				=> 'Menu',
	'category'			=> 'Categories',
	'viewallcat'		=> 'View all categories',
	'loremipsum'		=> 'Lorem Ipsum',
	'quickview'			=> 'Quick View',
	'designdevelop'		=> '2015 Designed &amp; Developed by Shaon'




];