				<div class="col-md-3">
					<div class="menu_box">
				   	  	<h3 class="menu_head"><?php Trans::text('menu'); ?></h3>

				   	  	<?php HTML::create_menu('arch/menu/menu.xml') ?>

			   	    </div>
			   	    <?php  $category = $_SESSION['data']; ?>
				    <div class="tags">
				    	<h4 class="tag_head"><?php Trans::text('category'); ?></h4>
				         <ul class="tags_links">
							<?php

								foreach ($category as $cat) {

									echo '<li><a href="#">';
									if(Trans::lang() == 'bn')
										echo $cat['cat_name_bn'];
									else
										echo $cat['cat_name'];
									echo '</a></li>';
								}

							?>

						</ul>
						<a href="#" class="link1"><?php Trans::text('viewallcat'); ?></a>
				     </div>
				</div>