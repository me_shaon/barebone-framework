<body>

<h1>Login Form</h1>

<div class="login-form">
	<div class="head-info">
		<label class="lbl-1"> </label>
		<label class="lbl-2"> </label>
		<label class="lbl-3"> </label>
	</div>
	<div class="clear"> </div>
	<div class="avtar">
		<?php HTML::image('public/images/avtar.png');  ?>
	</div>
	<form action="<?php echo APP_URL?>logincontroller/postlogin/" method="POST">
			<?php
				if(isset($GLOBALS['error']))
				{
					echo "<span style='color: #BD1515'>";
					echo $GLOBALS['error'];
					echo "</span>";
				}

				if(isset($GLOBALS['message']))
				{
					echo "<span style='color: #4DB256'>";
					echo $GLOBALS['message'];
					echo "</span>";
				}
			?>

			<input type="text" class="text" placeholder="Username" name="name" value="" required >
			<div class="key">
				<input name="password" class="pass" value="" type="password" placeholder="Password" required >
			</div>
	<div class="signin">
		<input type="submit" value="Login" >
	</div>
	</form>

</div>

</body>
</html>