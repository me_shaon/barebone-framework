	<div class="header">
	    <div class="container">
	        <div class="header-top">
	      		<div class="logo">
					<a href="">
					<?php HTML::image('public/images/logo.png',array('style'=>'margin-left: -6em;')); ?>
					</a>
				</div>

				    <div class="header_right">
					    <div class="lang-list pull-right" style="margin-top: -2em;">
					    <form name='langform' action="<?php echo APP_URL?>homecontroller/changelang/" method="GET">
						  <select name="lang" class="dropdown" id="langlist" tabindex="4" onchange="formSubmit();">
							<option <?php if(Trans::lang() == 'en') echo 'selected'; ?> value='en'>English</option>
							<option <?php if(Trans::lang() == 'bn') echo 'selected'; ?> value='bn'>Bangla</option>
						  </select>
						 </form>
			   			</div>
						<div class="clearfix"></div>
		            </div>

	          	<div class="clearfix"></div>
			</div>
			<div class="index-banner" style="position: relative; overflow: hidden;">
			   <article style="position: relative; width: 100%; opacity: 1;">
			   	   <div class="banner-wrap">
			   	      <div class="bannertop_box">
			   	      		<div class="welcome_box">
				  				<h2><?php Trans::text('welcome'); ?></h2>
				  				<p><?php Trans::text('welcomesubtext'); ?></p>
				  			</div>
			   		 		<ul class="login">

			   		 			<?php if(Auth::userLoggedIn())
			   		 				{
			   		 					echo '<li class="login_text">Hello '. Auth::get('name').'</li>';

			   		 			?>

			   		 			<li class="wish"><a href="logincontroller/logout"><?php Trans::text('logout'); ?></a></li>
			   		 			<?php
			   		 				}
			   		 				else
			   		 				{
			   		 			 ?>

			   		 			<li class="login_text"><a href="logincontroller/login"><?php Trans::text('login'); ?></a></li>
			   		 			<li class="wish"><a href="logincontroller/register"><?php Trans::text('register'); ?></a></li>
								<?php
									}
								?>

			   		 			<div class='clearfix'></div>
			   		 		</ul>
			   		 	</div>
			   		 	<div class="banner_right">
			   		 		<h1><?php Trans::text('bestbooks'); ?></h1>
			   		 		<p><?php Trans::text('bestbooksubtext'); ?></p>
			   		 		<a href="#" class="banner_btn"><?php Trans::text('buynow'); ?></a>
			   		 	</div>
			   		 	<div class='clearfix'></div>
			   		</div>
			   </article>
	      	</div>
		</div>
	</div>


	<script>

	function formSubmit(form) {
		var e = document.getElementById("langlist");
		var lang = e.options[e.selectedIndex].value;
		document['langform'].action = document['langform'].action + lang;
		document['langform'].submit();
	};

	</script>