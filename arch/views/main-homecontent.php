				<div class="col-md-9">
				   	<h3 class="m_2"><?php Trans::text('bestseller'); ?></h3>
				   	<div class="content_grid">
				   		<div class="col_1_of_3 span_1_of_3">
					  	 <div class="view view-first">
						      <a href="">
							   <div class="inner_content clearfix">
								<div class="product_image">
									<?php HTML::image('public/images/pic1.jpg',array('class'=>'img-responsive')); ?>
									<div class="mask">
			                       		<div class="info"><?php Trans::text('quickview'); ?></div>
					                  </div>
									<div class="product_container">
									   <div class="cart-left">
										 <p class="title"><?php Trans::text('loremipsum'); ?></p>
									   </div>
									   <div class="price">$99.00</div>
									   <div class="clearfix"></div>
								     </div>
								</div>
			                   </div>
			                 </a>
					       </div>
					    </div>
					    <div class="col_1_of_3 span_1_of_3">
					  	 <div class="view view-first">
						      <a href="">
							   <div class="inner_content clearfix">
								<div class="product_image">
									<?php HTML::image('public/images/pic2.jpg',array('class'=>'img-responsive')); ?>
									<div class="mask">
			                       		<div class="info"><?php Trans::text('quickview'); ?></div>
					                  </div>
									<div class="product_container">
									   <div class="cart-left">
										 <p class="title"><?php Trans::text('loremipsum'); ?></p>
									   </div>
									   <div class="price">$99.00</div>
									   <div class="clearfix"></div>
								     </div>
								</div>
			                   </div>
			                 </a>
					       </div>
					    </div>
					    <div class="col_1_of_3 span_1_of_3">
					  	 <div class="view view-first">
						      <a href="">
							   <div class="inner_content clearfix">
								<div class="product_image">
									<?php HTML::image('public/images/pic3.jpg',array('class'=>'img-responsive')); ?>
									<div class="mask">
			                       		<div class="info"><?php Trans::text('quickview'); ?></div>
					                  </div>
									<div class="product_container">
									   <div class="cart-left">
										 <p class="title"><?php Trans::text('loremipsum'); ?></p>
									   </div>
									   <div class="price">$99.00</div>
									   <div class="clearfix"></div>
								     </div>
								</div>
			                   </div>
			                 </a>
					       </div>
					    </div>

					    <div class="clearfix"></div>
				    </div>

			  	</div>